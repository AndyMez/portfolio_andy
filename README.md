# Portfolio

Durant la formation Développement Web et Mobile nous avons eu pour premier projet la réalisation de notre Portfolio responsive, puis le mettre sur Gitlab

Bien que je comprenne Bootstrap dans le fond, j'ai eu quelques difficultés a réalisé ma responsive. Pour éviter de perdre trop de temps là-dessus, j'ai décidé de conserver uniquement les éléments Bootstrap qui m'intéressait et j'ai fait ma responsive à l'aide des MediaQueries (Dont j'avais déjà quelques connaissances là-dessus)

Concernant mon Portfolio je me suis dirigé vers des couleurs plutôt pâles pour deux raisons :

* Car ce sont des thèmes de couleurs que j'affectionne particulièrement
* J'avais comme objectif la réalisation d'un thème Clair & Sombre,mais ayant eu quelques difficultés au cours de mon Portfolio de manière générale et ne souhaitant pas faire un simple copier/coller sans comprendre le code, j'ai pris la décision de ne créer cette fonctionnalité. D'autant plus que mon site n'en a pas besoin

Dans tous les cas, j'ajouterais cette fonctionnalité ainsi que d'avantages choses plus tard. Notamment lorsque j'aurais appris plus de choses sur JavaScript

Maquettes & Portfolio : \
[Mes Maquettes](/design). \
[Mon Portfolio](https://andymez.gitlab.io/portfolio_andy/).
