import { Link } from "react-router-dom";

const Navigation = () => {
  return (
    <nav className="navigation">
      <div className="navigation__wrapper">
        <div className="navigation__menu">
          <Link to="/" className="navigation__menu--item">Accueil</Link>
          <Link to="/about" className="navigation__menu--item">À propos</Link>
        </div>
      </div>
    </nav>
  );
};

export default Navigation;
