import Navigation from "../layout/Navigation";
import imageArt from "../images/imageArt.jpg";
import CV from "../assets/cv.pdf";

const About = () => {
  return (
    <div className="about">
      <Navigation />
      <div className="about__wrapper">
        <div className="about__content">
          <figure className="about__content__media">
            <img src={imageArt} alt="art" className="about__media__image" />
          </figure>
          <div className="about__content__informations">
            <h2 className="about__content__title">Oh non ... 😭</h2>
            <p className="about__content__details">
              Je suis actuellement en train de retravailler ce portfolio. Plus
              d'informations sur moi à venir ! (Dont le responsive, promis !)
            </p>
            <div className="about__contact__link">
              <p className="about__contact__link__title">
                Cependant, voici mes coordonnées :
              </p>
              <h3 className="about__contact__link__item">
                👉{" "}
                <a href={CV} target="_blank" rel="noreferrer noopener">
                  Télécharger mon CV
                </a>
              </h3>
              <h3 className="about__contact__link__item">
                👉{" "}
                <a
                  href="https://gitlab.com/AndyMez"
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  GitLab
                </a>
              </h3>
              <h3 className="about__contact__link__item">
                👉{" "}
                <a
                  href="https://www.linkedin.com/in/andy-mezence/"
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  LinkedIn
                </a>
              </h3>
              <h3 className="about__contact__link__item">
                👉 Mail : andy.mezence.simplon@gmail.com
              </h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;

// Ajouter l'image art
// <img src="" alt="" /> Ajouter image d'art.
// <h2>Retour à la page précédente</h2>
// <h1>Je suis actuellement en train de retravailler mon portfolio. Plus d'informations sur moi à venir !</h1>
// <p>Cependant, je vais regrouper toutes les informations utiles sur moi ! 😄</p>
