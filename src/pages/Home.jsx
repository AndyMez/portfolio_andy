import Navigation from "../layout/Navigation";
import Moi from "../images/moi.jpg";
import Pokemon from "../images/projet-pokemon.png";
import Blog from "../images/projet-blog.png";
import Budget from "../images/projet-budget.png";
import CV from "../assets/cv.pdf"
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className="home">
      <div className="home__wrapper">
        <header className="home__header">
          <Navigation />
          <div className="home__hero__content">
            <h1 className="hero__hero__name">Andy Mezence</h1>
            <h2 className="hero__hero__job">Développeur Web Junior</h2>
            <p className="hero__hero__label">
              En recherche d'une alternance de 12 mois à partir de mars 2022
              <br />
              <p className="sorry">
                PS : Désolé, ce portfolio a était refait récemment, le
                responsive n'est pas encore fait, ni la partie "À propos", mais
                vous pouvez y trouvez mes informations ...
              </p>
            </p>
            <p></p>
            <div className="home__explore">
              <span className="home__explore__text">Scroll</span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true"
                role="img"
                class="iconify iconify--teenyicons"
                width="32"
                height="32"
                preserveAspectRatio="xMidYMid meet"
                viewBox="0 0 15 15"
              >
                <path
                  fill="currentColor"
                  fill-rule="evenodd"
                  d="M8 1v11.293l3.146-3.147l.708.708L7.5 14.207L3.146 9.854l.708-.708L7 12.293V1h1Z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </div>
          </div>
        </header>

        <main className="home__main">
          <div className="home__main__wrapper">
            <div className="home__main__content">
              <div className="home__main__introduction">
                <figure className="home__main__introduction--media">
                  <img
                    src={Moi}
                    alt="moi"
                    className="home__main__introduction__image"
                  />
                </figure>
                <div className="home__main__introduction--content">
                  <h3 className="home__main__introduction--hello">
                    Salut ! 👋🏽
                  </h3>
                  <div className="home__main__introduction--paragraph">
                    <p className="introduction-current">
                      Je m'appelle Andy, je sors actuellement de 10 mois de
                      formation au sein de{" "}
                      <a
                        href="https://simplon.co/"
                        target="_blank"
                        rel="noreferrer noopener"
                        className="simplon"
                      >
                        Simplon
                      </a>{" "}
                      en tant que Développeur Web.
                    </p>
                    <p className="introduction-future">
                      Dans le but de continuer mon apprentissage, je suis
                      maintenant à la recherche d'une alternance en CDA
                      (Concepteur développeur d'application) pour une durée de
                      12 mois.
                    </p>
                    <br />

                    <p className="introduction-infos">
                      D'ailleurs, je suis éligible à la prime de 8000€ ! 💰
                      (Plutôt cool, non ?)
                    </p>
                    <br />
                  </div>
                  <div className="home__main__introduction--more">
                    <button>
                      <a href={CV} target="_blank" rel="noreferrer noopener">
                        Télécharger mon CV
                      </a>
                    </button>
                    <button>
                      <a
                        href="https://gitlab.com/AndyMez"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        GitLab
                      </a>
                    </button>
                    <button>
                      <a
                        href="https://www.linkedin.com/in/andy-mezence/"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        LinkedIn
                      </a>
                    </button>
                    <button>
                      <Link to="/about">À propos de moi</Link>
                    </button>
                  </div>
                </div>
              </div>

              <div className="home__main__skills">
                <div className="home__main__skills__curent">
                  <h2 className="home__main__skills__title">Compétences.</h2>
                  <div className="home__main__skills__list">
                    <h3 className="home__main__skills__item">
                      HTML - CSS - Javascript -
                    </h3>
                    <h3 className="home__main__skills__item">
                      SASS - ReactJS - Typescript -
                    </h3>
                    <h3 className="home__main__skills__item">
                      MySQL - NodeJS .
                    </h3>
                  </div>
                </div>
                <div className="home__main__skills__coming">
                  <h2 className="home__main__skills__title">
                    Souhaite apprendre. <span>(ou découvrir de mon côté)</span>
                  </h2>
                  <div className="home__main__skills__list">
                    <h3 className="home__main__skills__item">
                      ThreeJS - WebGL - GSAP -
                    </h3>
                    <h3 className="home__main__skills__item">
                      Et autre chose du genre .
                    </h3>
                  </div>
                </div>
              </div>

              <div className="home__main__projects">
                <div className="home__main__projects--title">
                  Projets.{" "}
                  <span>
                    (Plus de projets sur mon{" "}
                    <a
                      href="https://gitlab.com/AndyMez"
                      target="_blank"
                      rel="noreferrer noopener"
                    >
                      GitLab
                    </a>
                    )
                  </span>
                </div>
                <div className="home__main__projects--list">
                  <div className="home__main__projects--element">
                    <figure className="home__main__projets--media">
                      <a
                        href="https://andymez.gitlab.io/MiniGame/"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        <img
                          src={Pokemon}
                          alt="mini-game pokemon"
                          className="home__main__projects--image"
                        />
                      </a>
                    </figure>
                    <p className="home__main__projects--date">Juin 2021</p>
                    <p className="home__main__projects--description">
                      Projet - Mini Jeu Pokemon
                    </p>
                  </div>
                  <div className="home__main__projects--element">
                    <figure className="home__main__projets--media">
                      <a
                        href="https://andy-budget-app.netlify.app/"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        <img
                          src={Budget}
                          alt="budget app"
                          className="home__main__projects--image"
                        />
                      </a>
                    </figure>
                    <p className="home__main__projects--date">Juillet 2021</p>
                    <p className="home__main__projects--description">
                      Projet - Budget APP
                    </p>
                  </div>
                  <div className="home__main__projects--element">
                    <figure className="home__main__projets--media">
                      <a
                        href="https://project-blog-app.netlify.app/"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        <img
                          src={Blog}
                          alt="blog app"
                          className="home__main__projects--image"
                        />
                      </a>
                    </figure>
                    <p className="home__main__projects--date">Septembre 2021</p>
                    <p className="home__main__projects--description">
                      Projet - Blog APP
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>

        <footer></footer>
      </div>
    </div>
  );
};

export default Home;
